import yaml
from jinja2 import Template, FileSystemLoader, Environment
import humps
import os
import shutil
import sys

types_dict = {
    'string': 'std::string',
    'boolean': 'bool',
    'number': 'double',
    'array string': 'std::vector<std::string>',
}

path_to_api=sys.argv[1]
ep_position=sys.argv[2]

def recreate_dir(path):
    shutil.rmtree(path)
    os.mkdir(path)

def create_dir_if_not_exist(path):
    if not os.path.exists(path):
        os.mkdir(path)

def write_method_cpp(method, dir, group, title):
    file = dir + '/' + humps.decamelize(title) + '.cpp'
    if os.path.exists(file):
        return
        
    with open("templates/template_method_cpp", "r") as template_method:
        template_method_str=template_method.read()

    t = Environment(loader=FileSystemLoader("templates/")).from_string(template_method_str)
    output = t.render(method=method, group=group, title=title)
    with open(file, 'w') as f:
        f.write(output)  

def write_method_struct(namespace, parameters, dir, group, title):
    with open("templates/template_struct", "r") as template:
        template_struct = Template(template.read(), trim_blocks=True, lstrip_blocks=True)
    output = template_struct.render(parameters=parameters, namespace=namespace, group=group, title=title, define=humps.decamelize(group+'_'+namespace+title+'HPP').upper())
    with open(dir + '/' + humps.decamelize(title) + '.hpp', 'w') as f:
        f.write(output)  

def write_handler(template_name, methods, group, group_dir, ext):
    with open("templates/" + template_name, "r") as template_handler:
        template_handler_str=template_handler.read()

    t = Environment(loader=FileSystemLoader("templates/")).from_string(template_handler_str)
    output = t.render(parameters=methods[group], group=group)
    with open(group_dir + '/' + humps.decamelize(group) + ext, 'w') as f:
        f.write(output)  


with open(path_to_api+"/api.yaml", "r") as stream:
    try:
        shutil.copyfile("templates/api_handler.hpp", path_to_api+"/src/api/api_handler.hpp")

        methods = {}
        data = yaml.safe_load(stream)
        
        for param in data['paths']:
            sp = param.split('/')

            if not sp[2] in methods:
                methods[sp[2]] = []

            for mm in data['paths'][param]:
                method = mm
                methodOrig = method
                if method == 'delete':
                    method = 'delete_method'
                if len(sp) > 3:
                    method = method + '_' + sp[3]
                    methodOrig = methodOrig + '_' + sp[3]

                m_description = data['paths'][param][mm]

                title = humps.pascalize(method)
                bodyData = 'string'
                if 'requestBody' in m_description:
                    body = m_description['requestBody']
                    if 'content' in body:
                        if 'application/json' in body['content']:
                            bodyData = body['content']['application/json']['schema']['properties']
                            for payload_param in bodyData:
                                payload_param_value=bodyData[payload_param]
                                if payload_param_value['type'] == 'string' and 'default' in payload_param_value:
                                    payload_param_value['default'] = '"' + payload_param_value['default'] + '"'
                                if 'enum' in payload_param_value:
                                    payload_param_value['type'] = humps.pascalize(title + 'Enum_' + payload_param)
                                else:
                                    payload_param_value['type'] = types_dict[payload_param_value['type']]

                auth = 'no'
                if 'security' in m_description:
                    if '401' in m_description['responses']:
                        auth = 'required'
                    else:
                        auth = 'optional'

                check_recaptcha_V3 = 'false'

                if 'responses' in m_description:
                    if '423' in m_description['responses']:
                        check_recaptcha_V3 = 'true'       
                      


                query_params = {}

                if 'parameters' in m_description:
                    for qparam in m_description['parameters']:
                        param_type=''
                        if qparam['schema']['type'] == 'array':
                            param_type='array ' + qparam['schema']['items']['type']
                        else:
                            param_type=qparam['schema']['type']

                        query_params[qparam['name']] = {
                            'type': types_dict[param_type]
                        }

                        if 'enum' in qparam['schema']:
                            query_params[qparam['name']]['enum'] = qparam['schema']['enum']
                            query_params[qparam['name']]['type'] = humps.pascalize(title + 'Enum_' + qparam['name'])

                        if 'default' in qparam['schema']:
                            add = ''
                            if qparam['schema']['type'] == 'string':
                                add = '"'
                            query_params[qparam['name']]['default'] = add+qparam['schema']['default']+add

                methods[sp[2]].append({'query_params':query_params, 'methodOrig': methodOrig, 'method': method, 'method_cpp_name': humps.decamelize(method),'title': title, 'payload': bodyData, 'auth': auth, 'check_recaptcha_V3': check_recaptcha_V3})

        with open("templates/template_api_handler_cpp", "r") as template_handler_cpp:
            template_handler_cpp_ = Template(template_handler_cpp.read(), trim_blocks=True, lstrip_blocks=True)
        output = template_handler_cpp_.render(methods=methods, ep_position=ep_position)
        with open(path_to_api + '/src/api/api_handler.cpp', 'w') as f:
            f.write(output)  


        for group in methods:
            print (group)
            group_dir = path_to_api + '/src/api/' + group
            create_dir_if_not_exist(group_dir)
            group_data_models_dir = group_dir + '/data_models'
            create_dir_if_not_exist(group_data_models_dir)
            group_data_models_query_dir = group_data_models_dir + '/query'
            create_dir_if_not_exist(group_data_models_query_dir)
            group_data_models_payload_dir = group_data_models_dir + '/payload'
            create_dir_if_not_exist(group_data_models_payload_dir)

            group_methods_dir = group_dir + '/methods'
            create_dir_if_not_exist(group_methods_dir)

            print(methods[group])

            write_handler('template_handler', methods, group, group_dir, '.hpp')
            write_handler('template_handler_cpp', methods, group, group_dir, '.cpp')

            for method in methods[group]:
                write_method_cpp(method, group_methods_dir, group, method['title'])
                write_method_struct('query', method['query_params'], group_data_models_query_dir, group, method['title'])
                payload = method['payload']
                if payload != 'string':
                    write_method_struct('payload', payload, group_data_models_payload_dir, group, method['title'])
                        
    except yaml.YAMLError as exc:
        print(exc)