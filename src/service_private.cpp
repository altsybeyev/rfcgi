#include "service_private.h"

#include <boost/algorithm/string.hpp>
#include <fcgiapp.h>

#include <common_tools/boostlog.hpp>
#include <rfcgi/api_handler.hpp>
#include <rfcgi/iauthentificator.hpp>
#include <rfcgi/iendpoints_handler.hpp>
#include <rfcgi/robots_checker.hpp>
#include <rfcgi/utils.hpp>

namespace rfcgi
{
FCGIServicePrivate::FCGIServicePrivate(const FCGIServiceConfig&            config,
                                       std::unique_ptr<BaseApiHandler>&&   handler,
                                       std::unique_ptr<IAuthentificator>&& authentificator)
    : BaseService(config), handler_(std::move(handler)),
      authentificator_(std::move(authentificator)), terminate_(false)
{
    if (FCGX_Init() != 0)
    {
        throw std::runtime_error("FCGX_Init() failed");
    };
}
void FCGIServicePrivate::terminate()
{
    BL_FTRACE();
    terminate_ = true;
    for (auto& thread : threads_)
    {
        thread.join();
    }
    FCGX_ShutdownPending();
    close(fcgi_socket_);
}

FCGIServicePrivate::~FCGIServicePrivate()
{
    terminate();
}

void FCGIServicePrivate::start()
{
    Py_Initialize();
    PyEval_InitThreads();
    PyEval_ReleaseLock();

    PyEval_AcquireLock();
    main_python_thread_state = NULL;
    main_python_thread_state = PyThreadState_Get();
    PyEval_ReleaseLock();

    fcgi_socket_ = FCGX_OpenSocket(_config.fcgi_address.c_str(), _config.backlog_size);
    if (fcgi_socket_ < 0)
    {
        BL_ERROR() << "error create socket";
        return;
    }

    set_socker_permissions(_config.fcgi_address.c_str());

    for (size_t i = 0; i < _config.thread_count; ++i)
    {
        threads_.push_back(std::thread(&FCGIServicePrivate::thread_loop, this));
    }

    thread_loop();
}

void FCGIServicePrivate::thread_loop()
{
    PyEval_AcquireLock();
    PyInterpreterState* main_interpreter_state = main_python_thread_state->interp;
    PyThreadState*      python_thread_state    = PyThreadState_New(main_interpreter_state);
    PyEval_ReleaseLock();

    FCGX_Request request;
    if (FCGX_InitRequest(&request, fcgi_socket_, 0) != 0)
    {
        BL_ERROR() << "Error init FCGX request. Socket: " << fcgi_socket_;
        return;
    }
    while (!terminate_)
    {
        int accept_result;
        {
            std::lock_guard<std::mutex> lock(socket_lock_);
            accept_result = FCGX_Accept_r(&request);
        }

        if (accept_result < 0)
        {
            BL_ERROR() << "Error accept new request";
            break;
        }

        HttpResponse response;

        try
        {
            response = process_request(request, python_thread_state);
        }
        catch (...)
        {
            response.http_code = 500;
        }
        try
        {
            process_response(request, response);
        }
        catch (...)
        {
        }
    }
}

void FCGIServicePrivate::process_response(FCGX_Request& request, const HttpResponse& response)
{
    if (!response.headers.empty() || response.http_code != 200)
    {
        std::stringstream headers;

        if (response.http_code != 200)
        {
            headers << "Status: " + std::to_string(response.http_code) + "\r\n";
        }
        if (response.headers.find("Location") != response.headers.end())
        {
            headers << "Status: 302 Found\r\n";
        }
        else
        {
            headers << "Status: 200 OK\r\n";
        }

        for (auto iter : response.headers)
        {
            headers << iter.first << ": " << iter.second << "\r\n";
        }
        headers << "\r\n";

        FCGX_PutS(headers.str().c_str(), request.out);
        FCGX_PutStr(response.c_str(), static_cast<int>(response.size()), request.out);
    }
    else
    {
        BL_DEBUG() << "RESPONSE: ";
        BL_DEBUG() << response.c_str();

        const std::string json_headers = "Status: 200 OK \r\n"
                                         "Content-Type: application/json; charset=UTF-8\r\n"
                                         "\r\n";

        FCGX_PutS(json_headers.c_str(), request.out);
        FCGX_PutS(response.c_str(), request.out);
    }
    FCGX_Finish_r(&request);
}

HttpResponse FCGIServicePrivate::process_request(FCGX_Request&  request,
                                                 PyThreadState* python_thread_state)
{
    debug_fcgi_parameters(request.envp);

    const auto id_user = authentificator_->auth(request);

    const auto query = get_fcgi_param("QUERY_STRING", request.envp);

    const auto method =
        boost::algorithm::to_lower_copy(get_fcgi_param("REQUEST_METHOD", request.envp));

    const auto payload = get_payload(request, method);
    RETURN_ON_COND_CODE(!payload.second.empty(), 400, payload.second)

    auto endpoint = std::string(get_fcgi_param("SCRIPT_NAME", request.envp));
    RETURN_ON_COND_CODE(endpoint.empty(), 400, "empty endpoint")

    return handler_->execute(id_user, method, endpoint, query, payload.first, request,
                             python_thread_state);
}

} // namespace rfcgi