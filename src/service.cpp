#include <rfcgi/api_handler.hpp>
#include <rfcgi/iauthentificator.hpp>
#include <rfcgi/service.hpp>

#include "service_private.h"

namespace rfcgi
{

FCGIService::FCGIService(const FCGIServiceConfig& config, std::unique_ptr<BaseApiHandler>&& handler,
                         std::unique_ptr<IAuthentificator>&& authentificator)
    : impl_(std::make_shared<FCGIServicePrivate>(config, std::move(handler),
                                                 std::move(authentificator)))
{
}

void FCGIService::run()
{
    impl_->start();
}

} // namespace rfcgi
