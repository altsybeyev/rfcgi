
#include <rfcgi/iendpoints_handler.hpp>

#include <variant>

namespace rfcgi
{
    
IEndpointsHandler::IEndpointsHandler(const std::shared_ptr<IRobotsChecker>& checker)
    : checker_(checker)
{
}

HttpResponse IEndpointsHandler::execute(const std::unique_ptr<std::string>& id_user,
                                        const std::string& method, const std::string& path,
                                        const std::string& query_string, const std::string& payload,
                                        FCGX_Request&  request,
                                        PyThreadState* python_thread_state) noexcept
{
    const auto postfix = path == "" ? "" : "_" + path;

    auto it = operations.find(method + postfix);

    if (it != operations.end())
    {
        if (it->second.second)
        {
            const auto is_not_robot_result = checker_->check_is_not_robot(request);
            RETURN_ON_COND_CODE(!is_not_robot_result.second.empty(), is_not_robot_result.first,
                                is_not_robot_result.second)
        }
        const auto& ff = it->second.first;

        try
        {
            const auto func = std::get<login_optional_required_operation>(ff);
            return func(id_user, query_string, payload, request, python_thread_state);
        }
        catch (const std::bad_variant_access& ex)
        {
        }

        try
        {
            const auto func = std::get<no_login_required_operation>(ff);
            return func(query_string, payload, request, python_thread_state);
        }
        catch (const std::bad_variant_access& ex)
        {
        }
        try
        {
            const auto func = std::get<login_required_operation>(ff);
            if (id_user)
            {
                return func(*id_user, query_string, payload, request, python_thread_state);
            }
            else
            {
                RETURN_ON_COND_CODE_PAIR(401, "")
            }
        }
        catch (const std::bad_variant_access& ex)
        {
        }
        RETURN_ON_COND_CODE_PAIR(500, "")
    }

    RETURN_ON_COND_CODE_PAIR(404, "")
}
} // namespace rfcgi
