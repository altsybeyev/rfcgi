#include <rfcgi/common.hpp>
#include <rfcgi/recaptcha3checker.hpp>
#include <rfcgi/utils.hpp>

#include <common_tools/boostlog.hpp>
#include <common_tools/json_helper.h>
#include <webtools/curl_request.hpp>

namespace rfcgi
{

Recaptcha3Checker::Recaptcha3Checker(const std::map<std::string, std::string>& ids,
                                     const bool validate, const double threshold) noexcept
    : ids_(ids), validate_(validate), threshold_(threshold)
{
}

std::pair<int, std::string> Recaptcha3Checker::check_is_not_robot(FCGX_Request& request) noexcept
{
    std::pair<int, std::string> result;
    result.first = 400;

    auto host = get_host_with_port(request.envp);

    auto host_id_it = ids_.find(host);

    if (host_id_it == ids_.end())
    {
        result.second = "unsupported host: " + host;
        return result;
    }

    // if (host_id_it->second != "" && validate_)
    // {
    //     const auto v3tocken = get_fcgi_param("HTTP_RECAPTCHA3TOCKEN", request.envp);

    //     if (v3tocken.empty())
    //     {
    //         result.second = "empty recaptcha tocken header";
    //         return result;
    //     }

    //     const auto curl_result = webtools::perform_curl_request(std::string(
    //         "https://www.google.com/recaptcha/api/siteverify?secret=" + host_id_it->second +
    //         "&response=" + v3tocken));

    //     if (!curl_result.second.empty())
    //     {
    //         return result;
    //     }

    //     const auto is_sucess = commtools::read_sigle_json_value(curl_result.first, "success");

    //     if (is_sucess != "true")
    //     {
    //         result.second = "invalid recaptcha tocken";
    //         return result;
    //     }

    //     const auto score = std::stod(commtools::read_sigle_json_value(curl_result.first, "score"));
    //     if (score < threshold_)
    //     {
    //         BL_WARNING() << "recaptcha suspicios score: " << score;
    //     }

    //     // RETURN_ON_COND_CODE(score < 0.3, 403, "you are a robot")
    // }
    return result;
}
} // namespace rfcgi
