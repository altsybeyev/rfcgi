#ifndef RFCGI_SERVICE_PRIVATE
#define RFCGI_SERVICE_PRIVATE

#include <thread>

#include <Python.h>
#include <fcgiapp.h>

#include <base_service/base_service.hpp>

#include <rfcgi/common.hpp>
#include <rfcgi/config.hpp>

namespace rfcgi
{
class BaseApiHandler;
class IRobotsChecker;
class IAuthentificator;

class FCGIServicePrivate : public base_service::BaseService<FCGIServiceConfig>
{
  public:
    FCGIServicePrivate(const FCGIServiceConfig& config, std::unique_ptr<BaseApiHandler>&& handler,
                       std::unique_ptr<IAuthentificator>&& authentificator);
    virtual ~FCGIServicePrivate();

    void start();

    void terminate();

  private:
    void thread_loop();

    HttpResponse process_request(FCGX_Request& request_fcgx, PyThreadState* python_thread_state);

    void process_response(FCGX_Request& request, const HttpResponse& response);

    const std::unique_ptr<BaseApiHandler>   handler_;
    const std::unique_ptr<IAuthentificator> authentificator_;

    PyThreadState* main_python_thread_state;

    bool terminate_;

    std::mutex socket_lock_;

    std::vector<std::thread> threads_;

    int fcgi_socket_;
};
} // namespace rfcgi
#endif // RFCGI_SERVICE_PRIVATE
