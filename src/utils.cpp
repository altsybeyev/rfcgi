#include <rfcgi/utils.hpp>

#include <fcgiapp.h>
#include <sys/stat.h>
#include <unistd.h>

#include <common_tools/boostlog.hpp>

namespace rfcgi
{
void set_socker_permissions(const char* socketPath)
{
    if (access(socketPath, F_OK) != -1)
    {
        chmod(socketPath, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    }
}
void debug_fcgi_parameters(char** envp)
{
    for (int i = 0; envp[i] != NULL; i++)
    {
        BL_DEBUG() << envp[i];
    }
}
std::string get_fcgi_param(const std::string& param, char** env)
{
    std::string res;
    const auto  str = FCGX_GetParam(param.c_str(), env);
    if (str)
    {
        res.assign(str);
    }
    return res;
}

std::pair<std::string, std::string> get_payload(FCGX_Request&      request_fcgx,
                                                const std::string& method)
{
    std::pair<std::string, std::string> result;

    if (method == "get" || method == "delete")
    {
        return result;
    }

    int content_len = 0;

    const auto content_len_str = get_fcgi_param("CONTENT_LENGTH", request_fcgx.envp);
    if (content_len_str.empty())
    {
        result.second = "error get CONTENT_LENGTH header";
        return result;
    }
    const auto err_msg = "incorrect CONTENT_LENGTH: ";

    try
    {
        content_len = std::stoi(content_len_str);
    }
    catch (const std::exception& ex)
    {
        result.second = err_msg + content_len_str + " - " + ex.what();
        return result;
    }

    if (content_len <= 0)
    {
        result.second = err_msg + content_len_str;
        return result;
    }

    result.first.resize(content_len);
    FCGX_GetStr(const_cast<char*>(result.first.data()), static_cast<int>(result.first.size()),
                request_fcgx.in);

    BL_TRACE() << "Request payload: " << result.first;

    return result;
}

std::string get_accept_lang(char** env)
{
    return get_fcgi_param("HTTP_ACCEPT_LANGUAGE", env).substr(0, 2);
}

std::string get_host_with_port(char** env)
{
    const auto HTTP_HOST = get_fcgi_param("HTTP_HOST", env);

    return HTTP_HOST + ":" + get_fcgi_param("SERVER_PORT", env);
}

} // namespace rfcgi