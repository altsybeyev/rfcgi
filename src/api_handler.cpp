#include <rfcgi/api_handler.hpp>

#include <Python.h>
#include <fcgiapp.h>

#include <common_tools/algorithm.hpp>

#include <rfcgi/iendpoints_handler.hpp>

namespace rfcgi
{
BaseApiHandler::BaseApiHandler(const int ep_position) : ep_position_(ep_position)
{
}

void BaseApiHandler::register_endpoints_handler(
    const std::string& group, std::unique_ptr<IEndpointsHandler>&& handler) noexcept
{
    handlers.emplace(group, std::move(handler));
}

HttpResponse BaseApiHandler::execute(const std::unique_ptr<std::string>& id_user,
                                     const std::string& method, const std::string& endpoint,
                                     const std::string& query_string, const std::string& payload,
                                     FCGX_Request&  request,
                                     PyThreadState* python_thread_state) noexcept
{
    const auto parts = commtools::strsplit_adapter<std::vector<std::string>>(endpoint, "/");

    RETURN_ON_COND_CODE(parts.size() < ep_position_ + 1, 404, "");

    const auto it = handlers.find(parts[ep_position_]);

    RETURN_ON_COND_CODE(it == handlers.end(), 404, "");

    const auto path = parts.size() == ep_position_ + 2 ? parts[ep_position_ + 1] : "";

    return it->second->execute(id_user, method, path, query_string, payload, request,
                               python_thread_state);
}
} // namespace rfcgi
