
#include <functional>
#include <list>
#include <map>
#include <memory>

#include <serreflection/read_url.hpp>

#include <rfcgi/api_handler.hpp>
#include <rfcgi/robots_checker.hpp>

namespace api
{
class ApiHandler : public rfcgi::BaseApiHandler
{
  public:
    explicit ApiHandler(const std::shared_ptr<rfcgi::IRobotsChecker>& checker =
                            std::make_shared<rfcgi::IRobotsChecker>());
};

} // namespace api
