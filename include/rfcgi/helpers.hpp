
#ifndef RFCGI_HELPERS
#define RFCGI_HELPERS

#include <memory>
#include <string>

#include <serreflection/read_json.hpp>

namespace rfcgi
{
namespace
{
template <typename T>
void convert_payload_(const std::string& payload, std::unique_ptr<T>& res)
{
    res = srfl::read_json_string<T>(payload);
}

template <typename T>
T payload_dereference(const std::unique_ptr<T>& payload)
{
    return *payload;
}

std::string payload_dereference(const std::string& payload)
{
    return payload;
}

template <typename T, typename S>
struct is_string
{
    static const bool value = false;
};

template <class T, class Traits, class Alloc>
struct is_string<T, std::basic_string<T, Traits, Alloc>>
{
    static const bool value = true;
};

template <typename TPayload>
std::unique_ptr<TPayload> convert_payload(
    const std::string& payload,
    typename std::enable_if<!std::is_same<TPayload, std::string>::value>::type* = nullptr)
{
    std::unique_ptr<TPayload> res;
    convert_payload_(payload, res);
    return std::move(res);
}

template <typename TPayload>
std::string convert_payload(
    const std::string& payload,
    typename std::enable_if<std::is_same<TPayload, std::string>::value>::type* = nullptr)
{
    return payload;
}

bool check_payload(const std::string& payload)
{
    return true;
}
template <typename T>
bool check_payload(const std::unique_ptr<T>& payload)
{
    return payload != nullptr;
}
} // namespace
} // namespace rfcgi

#endif // RFCGI_HELPERS
