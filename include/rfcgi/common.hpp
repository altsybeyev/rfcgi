#ifndef RFCGI_BASE_ENDPOINT_GROUP_CONTROLLER
#define RFCGI_BASE_ENDPOINT_GROUP_CONTROLLER

#include <map>
#include <string>

namespace rfcgi
{

struct HttpResponse : public std::string
{
    std::multimap<std::string, std::string> headers;

    long http_code = 200;

    HttpResponse() = default;

    HttpResponse(const std::string& s) : std::string(s){};
};
} // namespace rfcgi
#endif // RFCGI_BASE_ENDPOINT_GROUP_CONTROLLER

#define RETURN_ON_COND_CODE_PAIR(code, error)                                                      \
    rfcgi::HttpResponse respcode(error);                                                           \
    BL_WARNING() << "error responce code " << code << " and message: " << error;                   \
    respcode.http_code = code;                                                                     \
    return respcode;

#define RETURN_ON_COND_CODE(cond, code, error)                                                     \
    if (cond)                                                                                      \
    {                                                                                              \
        RETURN_ON_COND_CODE_PAIR(code, error)                                                      \
    }
