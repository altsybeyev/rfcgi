#ifndef RFCGI_API_HANDLER
#define RFCGI_API_HANDLER

#include <functional>
#include <list>
#include <map>
#include <memory>

#include <Python.h>
#include <fcgiapp.h>

#include <serreflection/read_url.hpp>

#include <rfcgi/common.hpp>

namespace rfcgi
{
class IEndpointsHandler;
class BaseApiHandler
{
  private:
  protected:
    std::map<std::string, std::unique_ptr<IEndpointsHandler>> handlers;
    void register_endpoints_handler(const std::string&                   group,
                                    std::unique_ptr<IEndpointsHandler>&& handler) noexcept;

    const int ep_position_;

  public:
    explicit BaseApiHandler(const int ep_position);

    virtual ~BaseApiHandler() = default;

    //   protected:
    HttpResponse execute(const std::unique_ptr<std::string>& id_user, const std::string& method,
                         const std::string& endpoint, const std::string& query_string,
                         const std::string& payload, FCGX_Request& request,
                         PyThreadState* python_thread_state) noexcept;
};

} // namespace rfcgi

#endif
