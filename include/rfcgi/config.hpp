#ifndef CONFIG_RFCGI_HPP
#define CONFIG_RFCGI_HPP

#include <base_service/baseconfig.hpp>

namespace rfcgi
{
struct FCGIServiceConfig
{
    std::string fcgi_address;

    int thread_count;

    int backlog_size;
};
} // namespace rfcgi

SERIALIZIBLE_STRUCT(rfcgi::FCGIServiceConfig, srfl::CheckModes::FATAL,
                    (std::string, fcgi_address, DEF_D())(int, thread_count, srfl::nan, 1,
                                                         srfl::inf)(int, backlog_size, srfl::nan, 1,
                                                                    srfl::inf))

#define SER_BASE_FCGIServiceConfig()                                                               \
    (std::string, fcgi_address, DEF_D())(int, thread_count, srfl::nan, 1,                          \
                                         srfl::inf)(int, backlog_size, srfl::nan, 1, srfl::inf)

#endif
