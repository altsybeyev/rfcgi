#ifndef FCGI_SERVICE_HPP
#define FCGI_SERVICE_HPP

#include <rfcgi/config.hpp>
#include <rfcgi/iauthentificator.hpp>
#include <rfcgi/robots_checker.hpp>

namespace rfcgi
{
class FCGIServicePrivate;
class BaseApiHandler;
class FCGIService
{
  public:
    FCGIService(const FCGIServiceConfig& config, std::unique_ptr<BaseApiHandler>&& handler,
                std::unique_ptr<IAuthentificator>&& authentificatorr =
                    std::make_unique<IAuthentificator>());
    void run();
    virtual ~FCGIService() = default;

  private:
    const std::shared_ptr<FCGIServicePrivate> impl_;
};

} // namespace rfcgi

#endif
