
#ifndef RFCGI_IENDPOINTS_HANDLER
#define RFCGI_IENDPOINTS_HANDLER

#include <functional>
#include <list>
#include <map>
#include <memory>

#include <Python.h>
#include <fcgiapp.h>

#include <serreflection/read_url.hpp>

#include <rfcgi/common.hpp>
#include <rfcgi/helpers.hpp>
#include <rfcgi/robots_checker.hpp>

#include <variant>

namespace rfcgi
{

using login_required_operation = std::function<HttpResponse(
    const std::string& id_user, const std::string& query_string, const std::string& payload,
    FCGX_Request& request, PyThreadState* python_thread_state)>;

using no_login_required_operation =
    std::function<HttpResponse(const std::string& query_string, const std::string& payload,
                               FCGX_Request& request, PyThreadState* python_thread_state)>;

using login_optional_required_operation = std::function<HttpResponse(
    const std::unique_ptr<std::string>& id_user, const std::string& query_string,
    const std::string& payload, FCGX_Request& request, PyThreadState* python_thread_state)>;

class IEndpointsHandler
{
  public:
    IEndpointsHandler(const std::shared_ptr<IRobotsChecker>& checker);

  protected:
    std::map<std::string,
             std::pair<std::variant<login_required_operation, no_login_required_operation,
                                    login_optional_required_operation>,
                       bool>>
        operations;

    const std::shared_ptr<IRobotsChecker> checker_;

    template <typename TQuery, typename TPayload>
    void register_op(const std::string& code, const bool check_recaptcha_V3,
                     const std::function<HttpResponse(
                         const TQuery& arg, const TPayload& payload, FCGX_Request& request,
                         PyThreadState* python_thread_state)>& operation) noexcept
    {
        auto fn = [=](const std::string& query_string, const std::string& payload,
                      FCGX_Request& request, PyThreadState* python_thread_state) -> HttpResponse {
            const auto query_struct = srfl::read_url<TQuery>(query_string);
            RETURN_ON_COND_CODE(!query_struct, 400, "")
            const auto payload_struct = convert_payload<TPayload>(payload);
            RETURN_ON_COND_CODE(!check_payload(payload_struct), 400, "")
            return operation(*query_struct, payload_dereference(payload_struct), request,
                             python_thread_state);
        };
        operations.emplace(code, std::pair(fn, check_recaptcha_V3));
    }

    template <typename TUser, typename TQuery, typename TPayload>
    void register_op_user(
        const std::string& code, const bool check_recaptcha_V3,
        const std::function<HttpResponse(const TUser& id_user, const TQuery& arg,
                                         const TPayload& payload, FCGX_Request& request,
                                         PyThreadState* python_thread_state)>& operation) noexcept
    {
        auto fn = [=](const TUser& id_user, const std::string& query_string,
                      const std::string& payload, FCGX_Request& request,
                      PyThreadState* python_thread_state) -> HttpResponse {
            const auto query_struct = srfl::read_url<TQuery>(query_string);
            RETURN_ON_COND_CODE(!query_struct, 400, "")
            const auto payload_struct = convert_payload<TPayload>(payload);
            RETURN_ON_COND_CODE(!check_payload(payload_struct), 400, "")
            return operation(id_user, *query_struct, payload_dereference(payload_struct), request,
                             python_thread_state);
        };
        operations.emplace(code, std::pair(fn, check_recaptcha_V3));
    }

  public:
    virtual ~IEndpointsHandler() = default;

    //   protected:
    virtual HttpResponse execute(const std::unique_ptr<std::string>& id_user,
                                 const std::string& method, const std::string& path,
                                 const std::string& query_string, const std::string& payload,
                                 FCGX_Request&  request,
                                 PyThreadState* python_thread_state) noexcept;
};
} // namespace rfcgi

#define REGISTER_OPERATION(code, check_recaptcha_V3, function, TQuery, TPayload)                   \
    this->register_op<TQuery, TPayload>(code, check_recaptcha_V3,                                  \
                                        std::bind(std::mem_fn(&function), this,                    \
                                                  std::placeholders::_1, std::placeholders::_2,    \
                                                  std::placeholders::_3, std::placeholders::_4));

#define REGISTER_OPERATION_WITH_USER(code, check_recaptcha_V3, function, TUser, TQuery, TPayload)  \
    this->register_op_user<TUser, TQuery, TPayload>(                                               \
        code, check_recaptcha_V3,                                                                  \
        std::bind(std::mem_fn(&function), this, std::placeholders::_1, std::placeholders::_2,      \
                  std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));

#endif