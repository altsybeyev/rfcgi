#ifndef RFCGI_IAUTHENTIFICATOR_CHECKER
#define RFCGI_IAUTHENTIFICATOR_CHECKER

#include <memory>
#include <string>

#include <fcgiapp.h>

namespace rfcgi
{
class IAuthentificator
{
  public:
    virtual ~IAuthentificator() = default;

    virtual std::unique_ptr<std::string> auth(FCGX_Request& request);
};

} // namespace rfcgi

#endif
