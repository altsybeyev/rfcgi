#ifndef RFCGI_RECAPTCHA3_CHECKER
#define RFCGI_RECAPTCHA3_CHECKER

#include <map>
#include <string>

#include <fcgiapp.h>
#include <rfcgi/robots_checker.hpp>

namespace rfcgi
{
class Recaptcha3Checker : public IRobotsChecker
{
  public:
    Recaptcha3Checker(const std::map<std::string, std::string>& ids, const bool validate,
                      const double threshold) noexcept;
    virtual std::pair<int, std::string>
    check_is_not_robot(FCGX_Request& request) noexcept override final;

  private:
    const std::map<std::string, std::string> ids_;

    const bool   validate_;
    const double threshold_;
};

} // namespace rfcgi

#endif
