#ifndef RFCGI_UTILS
#define RFCGI_UTILS

#include <string>

#include <fcgiapp.h>

namespace rfcgi
{
void        set_socker_permissions(const char* socketPath);
void        debug_fcgi_parameters(char** envp);
std::string get_fcgi_param(const std::string& param, char** env);
std::string get_accept_lang(char** env);
std::string get_host_with_port(char** env);

std::pair<std::string, std::string> get_payload(FCGX_Request&      request_fcgx,
                                                const std::string& method);
} // namespace rfcgi

#endif
