#ifndef RFCGI_ROBOTS_CHECKER
#define RFCGI_ROBOTS_CHECKER

#include <string>
#include <utility>

#include <fcgiapp.h>

namespace rfcgi
{
class IRobotsChecker
{
  public:
    virtual ~IRobotsChecker() = default;

    virtual std::pair<int, std::string> check_is_not_robot(FCGX_Request& request);
};

} // namespace rfcgi

#endif
